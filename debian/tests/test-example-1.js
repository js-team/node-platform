
const assert1 = require('assert');

var platform = require('platform');

console.log("platform.name = ", platform.name)
console.log("platform.version = ", platform.version)
console.log("platform.layout = ", platform.layout)
console.log("platform.os = ", platform.os)
console.log("platform.description = ", platform.description)
console.log("platform.product = ", platform.product)
console.log("platform.manufacturer = ", platform.manufacturer)


var info = platform.parse('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7.2; en; rv:2.0) Gecko/20100101 Firefox/4.0 Opera 11.52');
console.log("info.name = ", info.name); // 'Opera'
assert1.equal(info.name, 'Opera', 'info.name wrong');
console.log("info.version = ", info.version); // '11.52' 
assert1.equal(info.version, '11.52', 'info.version wrong');
console.log("info.layout = ", info.layout); // 'Presto' 
assert1.equal(info.layout, 'Presto', 'info.layout wrong');
console.log("info.os = ", info.os); // 'Mac OS X 10.7.2' 
assert1.equal(info.os.architecture, 32, 'info.os.architecture wrong');
assert1.equal(info.os.family, 'OS X', 'info.os.family wrong');
assert1.equal(info.os.version, '10.7.2', 'info.os.version wrong');
console.log("info.os.toString() = ", info.os.toString()); // 'Mac OS X 10.7.2'
assert1.equal(info.os.toString(), 'OS X 10.7.2', 'info.os.toString() wrong');
console.log("info.description = ", info.description); // 'Opera 11.52 (identifying as Firefox 4.0) on Mac OS X 10.7.2'
assert1.equal(info.description, 'Opera 11.52 (identifying as Firefox 4.0) on OS X 10.7.2', 'info.description wrong');
